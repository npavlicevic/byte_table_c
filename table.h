#ifndef TABLE_H
#define TABLE_H
#include "queue.h"
typedef struct entry {
  ULONG id;
  void *property;
} Entry;

UBYTE entry_create(Entry **this, ULONG id, const void *property);
void entry_copy(Entry **dest, Entry *src);
UBYTE entry_equals(Entry *dest, Entry *src);
void entry_destroy(Entry **this);

typedef struct search_score {
  ULONG diameter;
  double paths;
  double weights;
} SearchScore;

UBYTE score_create(SearchScore **this);
void score_destroy(SearchScore **this);

typedef struct table {
  Entry **entries;
  ByteArray **edges;
  double size;
  double capacity;
  double load_factor;
  ULONG maxTries;
} Table;

UBYTE table_create(Table **this, double capacity);
void table_insert_id(Table *this, Entry *entry);
void table_resize(Table *this);
Entry* table_find(Table *this, ULONG id);
UBYTE table_exists(Table *this, ULONG id);
void table_remove(Table *this, ULONG id);
ULONG table_neighbours(Table *this, ULONG **neighbours, ULONG id);
void table_establish_edge(Table *this, ULONG from_id, ULONG to_id);
UBYTE table_edge_exists(Table *this, ULONG id_from, ULONG id_to);
void table_search(Table *this, SearchScore *score, ULONG id);
void table_destroy(Table **this);

UBYTE file_open(FILE **f, const char *filename, const char *mode);
UBYTE file_create_line(UBYTE **line, ULONG size);
UBYTE file_read_line(FILE *f, ULONG size, UBYTE *line);
void file_destroy_line(UBYTE **line);
void file_close(FILE **f);

UBYTE *token_first(char *str, const char *delim);
UBYTE *token_next(char *delim);
#endif
