#include "queue_test.h"

int main() {
  queue_create_test();
  queue_push_test();
  queue_push_range_test();
  queue_get_test();
  queue_destroy_test();
  return 0;
}
