#include "table_test.h"

int main() {
  entry_create_test();
  entry_equals_test();
  table_create_test();
  table_destroy_test();
  table_insert_id_test();
  table_find_test();
  table_neighbours_test();
  table_edge_exists_test();
  table_search_test();
  table_search_score_test();
  entry_destroy_test();
  file_open_test();
  file_read_line_test();
  file_destroy_line_test();
  file_close_test();
  token_test();
  return 0;
}
