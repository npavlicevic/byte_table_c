#ifndef BYTE_ARRAY_H
#define BYTE_ARRAY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0
#define UNUSED(x) (void)(x)

typedef unsigned char UBYTE;
typedef unsigned int UINT;
typedef unsigned long ULONG;
typedef long long DLONG;

typedef struct byte_array {
  UBYTE *bytes;
  UINT size;
  // todo change this to ULONG
  UINT capacity;
  // todo change this to ULONG
} ByteArray;

UBYTE byte_array_create(ByteArray **this, UINT capacity);
UBYTE byte_array_created(ByteArray *this);
void byte_array_copy(ByteArray **dest, ByteArray *src);
void byte_array_copy_content(ByteArray *dest, ByteArray *src);
UBYTE byte_array_equals(ByteArray *dest, ByteArray *src);
UBYTE byte_array_exists(ByteArray *this, UBYTE byte);
UBYTE byte_array_at(ByteArray *this, ULONG position);
void byte_array_resize(ByteArray *this);
void byte_array_append_single(ByteArray *this, UBYTE byte);
void byte_array_slice(ByteArray **this, ULONG from);
UBYTE byte_array_iterate_compact(ByteArray *this, UBYTE (*ptr_iterate)(UINT, ULONG, ULONG), ULONG bytes);
UBYTE byte_array_exists_call_compact(UINT position, ULONG value, ULONG bytes);
UBYTE byte_array_exists_compact(ByteArray *this, ULONG byte);
void byte_array_append_compact(ByteArray *this, ULONG bytes);
ULONG byte_array_first_compact(ByteArray *this);
void byte_array_pop_compact(ByteArray **this);
UINT byte_array_count_compact(ByteArray *this);
void byte_array_destroy(ByteArray **this);
#endif
