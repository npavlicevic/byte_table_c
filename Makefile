#
# Makefile 
# byte_table
#

CC=gcc
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic

FILES=byte_array.c byte_array_test.c main.c
FILES_QUEUE=byte_array.c queue.c queue_test.c main_queue.c
FILES_TABLE=byte_array.c queue.c table.c table_test.c main_table.c
CLEAN=main main_queue main_table

all: byteArray queue table

byteArray: ${FILES}
	${CC} ${CFLAGS} $^ -o main

queue: ${FILES_QUEUE}
	${CC} ${CFLAGS} $^ -o main_queue

table: ${FILES_TABLE}
	${CC} ${CFLAGS} $^ -o main_table

clean: ${CLEAN}
	rm $^
