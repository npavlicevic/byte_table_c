#include "byte_array.h"

/*
  Create byte array
*/
UBYTE byte_array_create(ByteArray **this, UINT capacity) {
  *this = (ByteArray*)calloc(1, sizeof(ByteArray));
  if(*this == NULL) {
    return FALSE;
  }
  (*this)->size = 0;
  (*this)->capacity = capacity;
  (*this)->bytes = (UBYTE *)calloc((*this)->capacity, sizeof(UBYTE));
  if((*this)->bytes == NULL) {
    free(*this);
    return FALSE;
  }
  return TRUE;
}
/*
  Check if byte array created
*/
UBYTE byte_array_created(ByteArray *this) {
  if(this == NULL) {
    return FALSE;
  }
  if(this->bytes == NULL) {
    return FALSE;
  }
  return TRUE;
}
/*
  Copy byte array
*/
void byte_array_copy(ByteArray **dest, ByteArray *src) {
  byte_array_create(dest, src->capacity);
  byte_array_copy_content(*dest, src);
}
/*
  Byte array copy content
*/
void byte_array_copy_content(ByteArray *dest, ByteArray *src) {
  memcpy(dest->bytes, src->bytes, src->capacity);
  dest->size = src->size;
}
/*
  Check if two byte arrays are equal
*/
UBYTE byte_array_equals(ByteArray *dest, ByteArray *src) {
  // todo this in parallel maybe
  if(dest->size != src->size) {
    return FALSE;
  }
  UINT i = 0;
  for(; i < src->size; i++) {
    if(dest->bytes[i] != src->bytes[i]) {
      return FALSE;
    }
  }
  return TRUE;
}
/*
  Check if byte exists in byte array
*/
UBYTE byte_array_exists(ByteArray *this, UBYTE byte) {
  // todo this in parallel
  if(!byte_array_created(this)) {
    return FALSE;
  }
  int i = this->size;
  while(i >= 0) {
    if(this->bytes[--i] == byte) {
      return TRUE;
    }
  }
  return FALSE;
}
/*
  Element at position
*/
UBYTE byte_array_at(ByteArray *this, ULONG position) {
  if(!byte_array_created(this)) {
    return FALSE;
  }
  if(position >= this->size) {
    return FALSE;
  }
  return this->bytes[position];
}
/*
  Resize byte array if capacity reached
*/
void byte_array_resize(ByteArray *this) {
  UINT capacity = 2 * (this->capacity + this->size);
  // todo this to ULONG
  UBYTE *bytes = (UBYTE*)calloc(capacity, sizeof(UBYTE));
  UINT i = 0;
  for(; i < this->capacity; i++) {
    bytes[i] = this->bytes[i];
  }
  free(this->bytes);
  this->bytes = bytes;
  this->capacity = capacity;
}
/*
  Byte array append single
*/
void byte_array_append_single(ByteArray *this, UBYTE byte) {
  if(!byte_array_created(this)) {
    return;
  }
  if(this->size + 1 > this->capacity) {
    byte_array_resize(this);
  }
  this->bytes[this->size++] = byte;
}
/*
  Slice byte array from some position 
*/
void byte_array_slice(ByteArray **this, ULONG from) {
  if(!byte_array_created(*this)) {
    return;
  }
  ByteArray *another;
  byte_array_create(&another, (*this)->capacity);
  ULONG i = from;
  for(; i < (*this)->size; i++) {
    byte_array_append_single(another, byte_array_at(*this, i));
  }
  if(!another->size) {
    return;
  }
  byte_array_destroy(this);
  *this = another;
}
/*
  Iterate over compacts
*/
UBYTE byte_array_iterate_compact(ByteArray *this, UBYTE (*ptr_iterate)(UINT, ULONG, ULONG), ULONG bytes) {
  UBYTE mask = 0x7f;
  UBYTE last_bit = 0x80;
  ULONG value = 0;
  UINT i = 0;
  while(i < this->size) {
    value = 0;
    while(this->bytes[i] & last_bit) {
      value |= (this->bytes[i++] & mask);
      value <<= 7;
    }
    value |= this->bytes[i++];
    if(ptr_iterate(i, value, bytes)) {
      return TRUE;
    }
  }
  return FALSE;
}
/*
  Call for exists compact
*/
UBYTE byte_array_exists_call_compact(UINT position, ULONG value, ULONG bytes) {
  UNUSED(position);
  return value == bytes;
}
/*
  Check compact exists
*/
UBYTE byte_array_exists_compact(ByteArray *this, ULONG bytes) {
  return byte_array_iterate_compact(this, byte_array_exists_call_compact, bytes);
}
/*
  Append compact ULONG to array
*/
void byte_array_append_compact(ByteArray *this, ULONG bytes) {
  if(!byte_array_created(this)) {
    return;
  }
  UBYTE mask = 0x7f;
  UBYTE last_bit = 0x80;
  UBYTE first = bytes & mask;
  UBYTE second = bytes>>7 & mask;
  UBYTE third = bytes>>14 & mask;
  UBYTE fourth = bytes>>21 & mask;
  UBYTE fifth = bytes>>28 & mask;
  UBYTE blocks = 0;
  if(fifth) {
    byte_array_append_single(this, fifth | last_bit);
    blocks++;
  }
  if(fourth || blocks) {
    byte_array_append_single(this, fourth | last_bit);
    blocks++;
  }
  if(third || blocks) {
    byte_array_append_single(this, third | last_bit);
    blocks++;
  }
  if(second || blocks) {
    byte_array_append_single(this, second | last_bit);
    blocks++;
  }
  if(first || blocks) {
    byte_array_append_single(this, first);
  }
}
/*
  Get first element from compact array
*/
ULONG byte_array_first_compact(ByteArray *this) {
  if(!byte_array_created(this)) {
    return FALSE;
  }
  UBYTE mask = 0x7f;
  UBYTE last_bit = 0x80;
  ULONG value = 0, i = 0;
  while(this->bytes[i] & last_bit) {
    value |= (this->bytes[i++] & mask);
    value <<= 7;
  }
  value |= this->bytes[i];
  return value;
}
/*
  Pop first from left from compact array
*/
void byte_array_pop_compact(ByteArray **this) {
  if(!byte_array_created(*this)) {
    return;
  }
  ULONG i = 0;
  UBYTE last_bit = 0x80;
  while((*this)->bytes[i++] & last_bit) {}
  if(i == (*this)->size) {
    byte_array_destroy(this);
    return;
  }
  byte_array_slice(this, i);
}
/*
  Count the number of compact entries
*/
UINT byte_array_count_compact(ByteArray *this) {
  if(!byte_array_created(this)) {
    return 0;
  }
  UBYTE last_bit = 0x80;
  UINT count = 0;
  UINT i = 0;
  for(; i < this->size; i++) {
    if(!(this->bytes[i] & last_bit)) {
      count++;
    }
  }
  return count;
}
/*
  Destroy byte array
*/
void byte_array_destroy(ByteArray **this) {
  if(*this == NULL) {
    return;
  }
  free((*this)->bytes);
  (*this)->bytes = NULL;
  free(*this);
  *this = NULL;
}
