#ifndef QUEUE_H
#define QUEUE

#include "byte_array.h"

typedef struct queue {
  ByteArray *entries;
  UINT size;
} Queue;

UBYTE queue_create(Queue **this);
void queue_push(Queue *this, ULONG value);
void queue_push_range(Queue *this, ULONG size, ULONG *values);
ULONG queue_get(Queue *this);
void queue_pop(Queue *this);
void queue_destroy(Queue **this);
#endif
