#include "byte_array_test.h"

/*
  Create byte array test
*/
void byte_array_create_test() {
  ByteArray *byte_array;
  byte_array_create(&byte_array, 1024*1024);
  assert(byte_array_created(byte_array) == TRUE);
  byte_array_destroy(&byte_array);
}
/*
  Element exists in byte array test
*/
void byte_array_exists_test() {
  ByteArray *byte_array;
  ULONG capacity = 1024*1024, i = 0;
  byte_array_create(&byte_array, capacity);
  for(; i < capacity; i++) {
    byte_array_append_single(byte_array, 1);
  }
  assert(byte_array_exists(byte_array, 1) == TRUE);
  byte_array_destroy(&byte_array);
}
/*
  Element at position test
*/
void byte_array_at_test() {
  ByteArray *byte_array;
  ULONG capacity = 1024*1024, i = 1, max = 256;
  UBYTE first = 0;
  byte_array_create(&byte_array, capacity);
  time_t t;
  srand((unsigned) time(&t));
  first = rand() % max;
  byte_array_append_single(byte_array, first);
  for(; i < capacity; i++) {
    byte_array_append_single(byte_array, rand() % max);
  }
  assert(byte_array_at(byte_array, 0) == first);
  byte_array_destroy(&byte_array);
}
/*
  Byte array slice test
*/
void byte_array_slice_test() {
  ByteArray *byte_array;
  ULONG capacity = 1024*1024, max = 256;
  UBYTE first = 0, second = 0;
  byte_array_create(&byte_array, capacity);
  time_t t;
  srand((unsigned) time(&t));
  first = rand() % max;
  byte_array_append_single(byte_array, first);
  second = rand() % max;
  byte_array_append_single(byte_array, second);
  assert(byte_array->size == 2);
  assert(byte_array_at(byte_array, 0) == first);
  byte_array_slice(&byte_array, 1);
  assert(byte_array_at(byte_array, 0) == second);
  assert(byte_array->size == 1);
  byte_array_destroy(&byte_array);
}
/*
  Element exists in compact array test
*/
void byte_array_exists_compact_test() {
  ByteArray *byte_array;
  ULONG capacity = 1024*1024, i = 0, value = 0, max = 4294967296;
  byte_array_create(&byte_array, capacity);
  time_t t;
  srand((unsigned) time(&t));
  for(; i < capacity; i++) {
    value = rand() % max;
    byte_array_append_compact(byte_array, value);
  }
  assert(byte_array_exists_compact(byte_array, value) == TRUE);
  byte_array_destroy(&byte_array);
}
/*
  First element in compact array test
*/
void byte_array_first_compact_test() {
  ByteArray *byte_array;
  ULONG capacity = 1024*1024, first = 0, second = 0, i = 1, max = 4294967296;
  byte_array_create(&byte_array, capacity);
  time_t t;
  srand((unsigned) time(&t));
  first = rand() % max;
  byte_array_append_compact(byte_array, first);
  for(; i < capacity; i++) {
    byte_array_append_compact(byte_array, rand() % max);
  }
  second = byte_array_first_compact(byte_array);
  assert(second == first);
  byte_array_destroy(&byte_array);
}
/*
  Pop from compact array test
*/
void byte_array_pop_compact_test() {
  ByteArray *byte_array;
  ULONG capacity = 1024*1024, first = 0, second = 0, max = 4294967296;
  byte_array_create(&byte_array, capacity);
  time_t t;
  srand((unsigned) time(&t));
  first = rand() % max;
  byte_array_append_compact(byte_array, first);
  second = rand() % max;
  byte_array_append_compact(byte_array, second);
  assert(byte_array_first_compact(byte_array) == first);
  byte_array_pop_compact(&byte_array);
  assert(byte_array_first_compact(byte_array) == second);
  byte_array_destroy(&byte_array);
}
/*
  Byte array count compact test
*/
void byte_array_count_compact_test() {
  ByteArray *byte_array;
  ULONG capacity = 1024*1024, max = 1024, i = 0;
  byte_array_create(&byte_array, capacity);
  time_t t;
  srand((unsigned)time(&t));
  for(; i < max; i++) {
    byte_array_append_compact(byte_array, rand() % capacity);
  }
  assert(byte_array_count_compact(byte_array) == max);
  byte_array_destroy(&byte_array);
}
/*
  Byte array equals test
*/
void byte_array_equals_test() {
  ByteArray *dest, *src;
  UINT capacity = 1024 * 1024;
  byte_array_create(&src, capacity);
  UINT i = 0;
  for(; i < capacity; i++) {
    byte_array_append_single(src, 1);
  }
  byte_array_copy(&dest, src);
  assert(byte_array_equals(dest, src));
  byte_array_destroy(&dest);
  byte_array_destroy(&src);
}
/*
  Destroy array test
*/
void byte_array_destroy_test() {
  ByteArray *byte_array;
  byte_array_create(&byte_array, 1024*1024);
  byte_array_destroy(&byte_array);
  assert(byte_array == NULL);
}
