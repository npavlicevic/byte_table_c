#include "queue_test.h"

/*
  Queue create test
*/
void queue_create_test() {
  Queue *queue;
  assert(queue_create(&queue) == TRUE);
  queue_destroy(&queue);
}
/*
  Queue push test
*/
void queue_push_test() {
  Queue *queue;
  queue_create(&queue);
  queue_push(queue, 1024);
  assert(queue->size == 1);
  queue_destroy(&queue);
}
/*
  Queue push range test
*/
void queue_push_range_test() {
  ULONG *values;
  ULONG capacity = 1024 * 1024;
  values = calloc(capacity, sizeof(ULONG));
  Queue *queue;
  queue_create(&queue);
  time_t t;
  srand((unsigned)time(&t));
  ULONG i = 0;
  for(; i < capacity; i++) {
    values[i] = rand() % capacity;
  }
  queue_push_range(queue, capacity, values);
  assert(queue->size);
  free(values);
  queue_destroy(&queue);
}
/*
  Queue get test
*/
void queue_get_test() {
  Queue *queue;
  queue_create(&queue);
  queue_push(queue, 1024);
  queue_push(queue, 2048);
  assert(queue_get(queue) == 1024);
  queue_destroy(&queue);
}
/*
  Queue pop test
*/
void queue_pop_test() {
  Queue *queue;
  queue_create(&queue);
  queue_push(queue, 1024);
  queue_push(queue, 2048);
  queue_pop(queue);
  assert(queue_get(queue) == 2048);
  queue_destroy(&queue);
}
/*
  Queue destroy test
*/
void queue_destroy_test() {
  Queue *queue = NULL;
  queue_create(&queue);
  queue_destroy(&queue);
  assert(queue == NULL);
}
