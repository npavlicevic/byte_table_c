#include "byte_array_test.h"

int main() {
  byte_array_create_test();
  byte_array_exists_test();
  byte_array_at_test();
  byte_array_slice_test();
  byte_array_exists_compact_test();
  byte_array_first_compact_test();
  byte_array_pop_compact_test();
  byte_array_count_compact_test();
  byte_array_equals_test();
  byte_array_destroy_test();
  return 0;
}
