#include "table_test.h"

/*
  Entry create test
*/
void entry_create_test() {
  Entry *entry;
  int *value = calloc(1, sizeof(int));
  *value = 1;
  assert(entry_create(&entry, 1, value) == TRUE);
  entry_destroy(&entry);
}
/*
  Check entries equal test
*/
void entry_equals_test() {
  Entry *dest, *src;
  int *value = calloc(1, sizeof(int));
  *value = 1;
  entry_create(&src, 1, value);
  entry_copy(&dest, src);
  assert(entry_equals(dest, src));
  entry_destroy(&dest);
  entry_destroy(&src);
}
/*
  Entry destroy test
*/
void entry_destroy_test() {
  Entry *entry;
  int *value = calloc(1, sizeof(int));
  *value = 1;
  entry_create(&entry, 1, value);
  entry_destroy(&entry);
  assert(entry == NULL);
}
/*
  Table create test
*/
void table_create_test() {
  Table *table = NULL;
  table_create(&table, 1024 * 1024);
  assert(table != NULL);
  table_destroy(&table);
}
/*
  Table insert id test
*/
void table_insert_id_test() {
  Entry *entry;
  Table *table;
  double capacity = 1024 * 1024;
  table_create(&table, capacity);
  ULONG i = 0;
  int *value;
  for(; i < capacity; i++) {
    value = calloc(1, sizeof(int));
    *value = i;
    entry_create(&entry, i, value);
    table_insert_id(table, entry);
  }
  assert(table_exists(table, capacity - 1) == TRUE);
  table_destroy(&table);
}
/*
  Find element in table
*/
void table_find_test() {
  Entry *entry;
  Table *table;
  double capacity = 1024 * 1024;
  table_create(&table, capacity);
  ULONG i = 0;
  int *value;
  for(; i < capacity; i++) {
    value = calloc(1, sizeof(int));
    *value = i;
    entry_create(&entry, i, value);
    table_insert_id(table, entry);
  }
  entry = table_find(table, capacity - 1);
  assert(entry->id == capacity - 1);
  table_destroy(&table);
}
/*
  Node neighbours test
*/
void table_neighbours_test() {
  Table *table;
  double capacity = 1024 * 1024;
  table_create(&table, capacity);
  Entry *from, *to;
  ULONG *neighbours;
  time_t t;
  srand((unsigned)time(&t));
  int *value;
  ULONG i = 0;
  for(; i < capacity / 4; i++) {
    value = calloc(1, sizeof(int));
    *value = 1;
    entry_create(&from, rand() % (ULONG)(capacity / 2), value);
    table_insert_id(table, from);
    value = calloc(1, sizeof(int));
    *value = 1;
    entry_create(&to, rand() % (ULONG)(capacity / 2) + (capacity / 2), value);
    table_insert_id(table, to);
    table_establish_edge(table, from->id, to->id);
  }
  table_neighbours(table, &neighbours, from->id);
  assert(sizeof(neighbours) / sizeof(neighbours[0]));
  free(neighbours);
  table_destroy(&table);
}
/*
  Check edge exists between two nodes test
*/
void table_edge_exists_test() {
  Entry *from, *to;
  double capacity = 1024 * 1024;
  Table *table;
  table_create(&table, capacity);
  int *value;
  time_t t;
  srand((unsigned)time(&t));
  ULONG i = 0;
  for(; i < capacity / 4; i++) {
    value = (int*)calloc(1, sizeof(int));
    *value = 1;
    entry_create(&from, rand() % (ULONG)(capacity / 2), value);
    table_insert_id(table, from);
    value = (int*)calloc(1, sizeof(ULONG));
    *value = 1;
    entry_create(&to, (rand() % (ULONG)(capacity / 2)) + (ULONG)(capacity / 2), value);
    table_insert_id(table, to);
    table_establish_edge(table, from->id, to->id);
  }
  assert(table_edge_exists(table, from->id, to->id));
  table_destroy(&table);
}
/*
  Table search test
*/
void table_search_test() {
  // temp graph
  // todo remove
  // 9 - 10 11 12
  // 10 - 9
  // 11 - 9 12
  // 12 - 9 11
  // todo see how to generate random graph
  // todo or use larger real graph
  Table *table;
  double capacity = 1024 * 1024;
  table_create(&table, capacity);
  Entry *nine, *ten, *eleven, *twelve;
  entry_create(&nine, 9, (int*)calloc(1, sizeof(int)));
  table_insert_id(table, nine);
  entry_create(&ten, 10, (int*)calloc(1, sizeof(int)));
  table_insert_id(table, ten);
  entry_create(&eleven, 11, (int*)calloc(1, sizeof(int)));
  table_insert_id(table, eleven);
  entry_create(&twelve, 12, (int*)calloc(1, sizeof(int)));
  table_insert_id(table, twelve);
  table_establish_edge(table, 9, 10);
  table_establish_edge(table, 9, 11);
  table_establish_edge(table, 9, 12);
  table_establish_edge(table, 10, 9);
  table_establish_edge(table, 11, 9);
  table_establish_edge(table, 11, 12);
  table_establish_edge(table, 12, 9);
  table_establish_edge(table, 12, 11);
  SearchScore *score;
  score_create(&score);
  table_search(table, score, 9);
  assert(score->diameter);
  score_destroy(&score);
  table_destroy(&table);
}
/*
  Table search score test
*/
void table_search_score_test() {
  SearchScore *search_score;
  score_create(&search_score);
  assert(search_score->diameter);
  score_destroy(&search_score);
}
/*
  Table destroy test
*/
void table_destroy_test() {
  Table *table;
  table_create(&table, 1024 * 1024);
  table_destroy(&table);
  assert(table == NULL);
}
/*
  File open test
*/
void file_open_test() {
  FILE *f;
  file_open(&f, "/home/real/go_apps/byte_table/data_skitter", "r");
  assert(f != NULL);
  file_close(&f);
}
/*
  File read line test
*/
void file_read_line_test() {
  FILE *f;
  file_open(&f, "/home/real/go_apps/byte_table/data_skitter", "r");
  UBYTE *line;
  file_create_line(&line, 1024);
  UBYTE read = file_read_line(f, 1024, line);
  assert(read);
  file_destroy_line(&line);
  file_close(&f);
}
/*
  File destroy line test
*/
void file_destroy_line_test() {
  FILE *f;
  file_open(&f, "/home/real/go_apps/byte_table/data_skitter", "r");
  UBYTE *line;
  file_destroy_line(&line);
  assert(!line);
  file_close(&f);
}
/*
  File close test
*/
void file_close_test() {
  FILE *f;
  file_open(&f, "/home/real/go_apps/byte_table/data_skitter", "r");
  file_close(&f);
  assert(f == NULL);
}
/*
  Token test
*/
void token_test() {
  FILE *f;
  file_open(&f, "/home/real/go_apps/byte_table/data_skitter", "r");
  UBYTE *line;
  file_create_line(&line, 1024);
  file_read_line(f, 1024, line);
  assert(token_first((char*)line, "\t"));
  assert(token_next("\t"));
  file_destroy_line(&line);
  file_close(&f);
}
