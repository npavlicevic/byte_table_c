#include "table.h"

/*
  Entry create
*/
UBYTE entry_create(Entry **this, ULONG id, const void *property) {
  // todo return pointer
  *this = (Entry*)calloc(1, sizeof(Entry));
  if(*this == NULL) {
    return FALSE;
  }
  (*this)->id = id;
  (*this)->property = (void *)property;
  return TRUE;
}
/*
  Copy the entry
*/
void entry_copy(Entry **dest, Entry *src) {
  entry_create(dest, src->id, src->property);
}
/*
  Check two entries equal
*/
UBYTE entry_equals(Entry *dest, Entry *src) {
  return dest->id == src->id;
}
/*
  Entry destroy
*/
void entry_destroy(Entry **this) {
  if(*this == NULL) {
    return;
  }
  free(*this);
  *this = NULL;
}
/*
  Create search score
*/
UBYTE score_create(SearchScore **this) {
  *this = (SearchScore*)calloc(1, sizeof(SearchScore));
  if(*this == NULL) {
    return FALSE;
  }
  (*this)->diameter = 1;
  (*this)->weights = 0;
  (*this)->paths = 0;
  return TRUE;
}
/*
  Destroy search score
*/
void score_destroy(SearchScore **this) {
  if(*this == NULL) {
    return;
  }
  free(*this);
  *this = NULL;
}
/*
  Create table
*/
UBYTE table_create(Table **this, double capacity) {
  *this = (Table*)calloc(1, sizeof(Table));
  if(*this == NULL) {
    return FALSE;
  }
  (*this)->size = 0;
  (*this)->capacity = capacity;
  (*this)->load_factor = 0.5;
  (*this)->maxTries = 65536;
  (*this)->entries = (Entry**)calloc((*this)->capacity, sizeof(Entry*));
  (*this)->edges = (ByteArray**)calloc((*this)->capacity, sizeof(ByteArray*));
  return TRUE;
}
/*
  Insert entry to table according to id
*/
void table_insert_id(Table *this, Entry *entry) {
  if(this == NULL) {
    return;
  }
  if(this->entries[entry->id]) {
    return;
  }
  if(this->size / this->capacity > this->load_factor) {
    table_resize(this);
  }
  this->entries[entry->id] = entry;
  this->size++;
}
/*
  Resize table
*/
void table_resize(Table *this) {
  if(this == NULL) {
    return;
  }
  double capacity = 2 * (this->capacity + this->size);
  Entry **entries = (Entry**)calloc(capacity, sizeof(Entry*));
  ByteArray **edges = (ByteArray**)calloc(capacity, sizeof(ByteArray*));
  Entry *entry;
  ByteArray *_edges;
  ULONG i = 0;
  for(; i < this->capacity; i++) {
    if(this->entries[i]) {
      entry_copy(&entry, this->entries[i]);
      entry_destroy(&this->entries[i]);
      entries[i] = entry;
    }
    if(this->edges[i]) {
      byte_array_copy(&_edges, this->edges[i]);
      byte_array_destroy(&this->edges[i]);
      edges[i] = _edges;
    }
  }
  free(this->entries);
  this->entries = entries;
  free(this->edges);
  this->edges = edges;
  this->capacity = capacity;
}
/*
  Find element in table
*/
Entry* table_find(Table *this, ULONG id) {
  if(this == NULL) {
    return NULL;
  }
  if(id >= this->capacity) {
    return NULL;
  }
  return this->entries[id];
}
/*
  Entry exists in table
*/
UBYTE table_exists(Table *this, ULONG id) {
  if(this == NULL) {
    return FALSE;
  }
  return this->entries[id] != NULL;
}
/*
  Remove entry and edges from table
*/
void table_remove(Table *this, ULONG id) {
  if(this == NULL) {
    return;
  }
  if(!this->entries[id]) {
    return;
  }
  entry_destroy(&this->entries[id]);
  byte_array_destroy(&this->edges[id]);
  this->size--;
}
/*
  Find node's neighbours
*/
ULONG table_neighbours(Table *this, ULONG **neighbours, ULONG id) {
  UINT at = 0;
  if(this == NULL) {
    return at;
  }
  if(!this->edges[id]) {
    return at;
  }
  UINT count = byte_array_count_compact(this->edges[id]);
  *neighbours = (ULONG*)calloc(count, sizeof(ULONG));
  if(neighbours == NULL) {
    return at;
  }
  ByteArray *edges;
  byte_array_copy(&edges, this->edges[id]);
  if(edges == NULL) {
    return at;
  }
  UINT first = 0;
  while(edges) {
    first = byte_array_first_compact(edges);
    byte_array_pop_compact(&edges);
    (*neighbours)[at++] = first;
  }
  return at;
}
/*
  Establish edge between two nodes
*/
void table_establish_edge(Table *this, ULONG id_from, ULONG id_to) {
  if(this == NULL) {
    return;
  }
  if(!this->entries[id_from]) {
    return;
  }
  if(!this->entries[id_to]) {
    return;
  }
  if(!this->edges[id_from]) {
    byte_array_create(&this->edges[id_from], 8);
  }
  byte_array_append_compact(this->edges[id_from], id_to);
}
/*
  Check if there is edge between two nodes
*/
UBYTE table_edge_exists(Table *this, ULONG id_from, ULONG id_to) {
  if(this == NULL) {
    return FALSE;
  }
  if(!this->edges[id_from]) {
    return FALSE;
  }
  return byte_array_exists_compact(this->edges[id_from], id_to);
}
/*
  Search table breadth first
*/
void table_search(Table *this, SearchScore *score, ULONG id) {
  // todo maybe matrix search or linear search
  if(this == NULL) {
    // todo use just !this
    return;
  }
  if(!this->edges[id]) {
    return;
  }
  Queue *queue;
  queue_create(&queue);
  ULONG *neighbours;
  ULONG degree = table_neighbours(this, &neighbours, id);
  queue_push_range(queue, degree, neighbours);
  ULONG size = queue->size;
  free(neighbours);
  ULONG first = 0;
  while(queue->size) {
    first = queue_get(queue);
    degree = table_neighbours(this, &neighbours, first);
    table_remove(this, first);
    if(degree) {
      queue_push_range(queue, degree, neighbours);
      free(neighbours);
    }
    queue_pop(queue);
    score->paths++;
    score->weights += score->diameter;
    if(!(--size)) {
      size = queue->size;
      score->diameter++;
    }
  }
  queue_destroy(&queue);
}
/*
  Destroy table
*/
void table_destroy(Table **this) {
  if(*this == NULL) {
    return;
  }
  ULONG i = 0;
  for(; i < (*this)->capacity; i++) {
    if(!(*this)->size) {
      break;
    }
    // todo do i need this?
    if((*this)->entries[i]) {
      entry_destroy(&(*this)->entries[i]);
    }
    if((*this)->edges[i]) {
      byte_array_destroy(&(*this)->edges[i]);
    }
  }
  free((*this)->entries);
  free((*this)->edges);
  free(*this);
  *this = NULL;
}
/*
  File open
*/
UBYTE file_open(FILE **f, const char *filename, const char *mode) {
  *f = fopen(filename, mode);
  if(*f == NULL) {
    return FALSE;
  }
  return TRUE;
}
/*
  Create string line
*/
UBYTE file_create_line(UBYTE **line, ULONG size) {
  *line = (UBYTE*)calloc(size, sizeof(UBYTE));
  if(*line == NULL) {
    return FALSE;
  }
  return TRUE;
}
/*
  Read line from file
*/
UBYTE file_read_line(FILE *f, ULONG size, UBYTE *line) {
  char *read = fgets((char*)line, size, f);
  if(read == NULL) {
    return FALSE;
  }
  return TRUE;
}
/*
  Destroy line
*/
void file_destroy_line(UBYTE **line) {
  if(*line == NULL) {
    return;
  }
  free(*line);
  *line = NULL;
}
/*
  File close
*/
void file_close(FILE **f) {
  if(*f == NULL) {
    return;
  }
  fclose(*f);
  *f = NULL;
}
/*
  Token first
*/
UBYTE *token_first(char *str, const char *delim) {
  return (UBYTE*)strtok(str, delim);
}
/*
  Token next
*/
UBYTE *token_next(char *delim) {
  return (UBYTE*)strtok(NULL, delim);
}
