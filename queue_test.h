#ifndef QUEUE_TEST_H
#define QUEUE_TEST_H

#include <assert.h>
#include <time.h>
#include "queue.h"

void queue_create_test();
void queue_push_test();
void queue_push_range_test();
void queue_get_test();
void queue_pop_test();
void queue_destroy_test();
#endif
