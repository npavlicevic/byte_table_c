#include <assert.h>
#include <time.h>
#include "byte_array.h"

void byte_array_create_test();
void byte_array_exists_test();
void byte_array_at_test();
void byte_array_slice_test();
void byte_array_exists_compact_test();
void byte_array_first_compact_test();
void byte_array_pop_compact_test();
void byte_array_count_compact_test();
void byte_array_equals_test();
void byte_array_destroy_test();
