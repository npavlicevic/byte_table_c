#include "queue.h"

/*
  Create queue
*/
UBYTE queue_create(Queue **this) {
  *this = (Queue*)calloc(1, sizeof(Queue));
  if(*this == NULL) {
    return FALSE;
  }
  (*this)->size = 0;
  ByteArray *byte_array = NULL;
  byte_array_create(&byte_array, 32);
  if(byte_array == NULL) {
    free(*this);
    return FALSE;
  }
  (*this)->entries = byte_array;
  return TRUE;
}
/*
  Push value to queue
*/
void queue_push(Queue *this, ULONG value) {
  byte_array_append_compact(this->entries, value);
  this->size++;
}
/*
  Queue push range
*/
void queue_push_range(Queue *this, ULONG size, ULONG *values) {
  if(this == NULL) {
    return;
  }
  if(!size) {
    return;
  }
  ULONG i = 0;
  for(; i < size; i++) {
    queue_push(this, values[i]);
  }
}
/*
  Get first value from queue
*/
ULONG queue_get(Queue *this) {
  if(!this->size) {
    return FALSE;
  }
  return byte_array_first_compact(this->entries);
}
/*
  Pop first from queue
*/
void queue_pop(Queue *this) {
  if(!this->size) {
    return;
  }
  ByteArray *entries = this->entries;
  byte_array_pop_compact(&entries);
  this->size--;
}
/*
  Destroy queue
*/
void queue_destroy(Queue **this) {
  ByteArray *entries = (*this)->entries;
  byte_array_destroy(&entries);
  free(*this);
  *this = NULL;
}
